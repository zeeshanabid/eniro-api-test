package com.eniro.api.search.searcher;

import com.eniro.api.response.EniroApiResponse;
import com.eniro.api.response.ResponseAggregator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * API caller for Eniro. This creates a GET request and pass all the required search parameters to make a search on
 * Eniro server.
 *
 * Created by zeeshan
 */
@Component
public class EniroSearcher {

    @Value("${eniro.profile}")
    private String profile;

    @Value("${eniro.key}")
    private String key;

    @Value("${eniro.country}")
    private String country;

    @Value("${eniro.api.version}")
    private String version;

    @Value("${eniro.search.url}")
    private String url;

    public ResponseAggregator search(String[] words) {
        Map<String, String> params = new HashMap<>();
        params.put("profile", profile);
        params.put("key",     key    );
        params.put("country", country);
        params.put("version", version);

        ResponseAggregator aggregator = new ResponseAggregator();
        for (String word : words) {
            params.put("search_word", word);
            String           queryParams  = "?profile={profile}&key={key}&country={country}&version={version}&search_word={search_word}";
            RestTemplate     restTemplate = createTemplate();
            EniroApiResponse response     = restTemplate.getForObject(url + queryParams, EniroApiResponse.class, params);
            aggregator.add(response.getAdverts());
        }
        return aggregator;
    }

    protected RestTemplate createTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate;
    }
}

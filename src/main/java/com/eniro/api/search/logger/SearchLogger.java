package com.eniro.api.search.logger;

import org.springframework.stereotype.Component;

import java.util.*;

/**
 * A class to maintain all the search log objects. This class can perform range operations to retrieve search logs
 * based on time interval
 *
 * Created by zeeshan
 */
@Component
public class SearchLogger {
    private final NavigableMap<Date, SearchLog> searchLog;

    public SearchLogger() {
        this.searchLog = new TreeMap<>();
    }

    public void log(final String[] searchWords, final String[] fields) {
        Date      d = getDate();
        SearchLog l = new SearchLog(searchWords, fields, d);
        synchronized (this.searchLog) {
            this.searchLog.put(d, l);
        }
    }

    protected Date getDate() {
        return new Date();
    }

    public List<SearchLog> getLog(Date from, Date to) {
        List<SearchLog> log = new ArrayList<>();
        synchronized (this.searchLog) {
            NavigableMap<Date, SearchLog> logMap = this.searchLog.subMap(from, true, to, true);
            log.addAll(logMap.values());
        }
        return log;
    }
}

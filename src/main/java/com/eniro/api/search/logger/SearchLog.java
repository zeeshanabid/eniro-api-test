package com.eniro.api.search.logger;

import com.eniro.api.utils.Utils;

import java.util.Arrays;
import java.util.Date;

/**
 * A search log object to store the search entries.
 *
 * Created by zeeshan
 */
public class SearchLog {
    private final String[] searchWords;
    private final String[] fields;
    private final Date date;

    public SearchLog(String[] searchWords, String[] fields, Date date) {
        this.searchWords = searchWords;
        this.fields      = fields;
        this.date        = date;
    }

    public String[] getSearchWords() {
        return Arrays.copyOf(this.searchWords, this.searchWords.length);
    }

    public String[] getFields() {
        return Arrays.copyOf(this.fields, this.fields.length);
    }

    public String getDate() {
        return Utils.formatDate(this.date);
    }

    @Override
    public String toString() {
        return "Search{words=" + Arrays.toString(this.searchWords) +
                ", fields=" + Arrays.toString(fields) + "}";
    }
}

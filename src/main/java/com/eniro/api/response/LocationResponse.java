package com.eniro.api.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Eniro Response class for Location
 * Created by zeeshan
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class LocationResponse {
    private CoordinateResponse[] coordinates;

    public CoordinateResponse[] getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(CoordinateResponse[] coordinates) {
        this.coordinates = coordinates;
    }
}

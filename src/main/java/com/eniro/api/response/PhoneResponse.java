package com.eniro.api.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Eniro Response class for Phone
 * Created by zeeshan
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PhoneResponse {
    private String type;
    private String phoneNumber;
    private String label;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return "Phone{number=" + this.phoneNumber + "}";
    }
}

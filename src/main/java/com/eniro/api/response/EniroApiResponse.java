package com.eniro.api.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Eniro Response class for the root object
 * Created by zeeshan
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EniroApiResponse {
    private String           title;
    private String           query;
    private int              totalHits;
    private int              totalCount;
    private int              startIndex;
    private int              itemsPerPage;
    private AdvertResponse[] adverts;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public int getTotalHits() {
        return totalHits;
    }

    public void setTotalHits(int totalHits) {
        this.totalHits = totalHits;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getItemsPerPage() {
        return itemsPerPage;
    }

    public void setItemsPerPage(int itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }

    public AdvertResponse[] getAdverts() {
        return adverts;
    }

    public void setAdverts(AdvertResponse[] adverts) {
        this.adverts = adverts;
    }

    @Override
    public String toString() {
        return "EniroSearchApiResponse{" +
                "title='" + title + '\'' +
                ", query='" + query + '\'' +
                ", totalHits=" + totalHits +
                ", startIndex=" + startIndex +
                ", itemsPerPage=" + itemsPerPage +
                '}';
    }
}

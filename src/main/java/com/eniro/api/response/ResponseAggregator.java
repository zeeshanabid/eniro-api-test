package com.eniro.api.response;

import java.util.*;

/**
 * Aggregator for adverts received from eniro. This class will aggregate all adverts for the searches made for multiple
 * word in a single API call.
 *
 * Created by zeeshan
 */
public final class ResponseAggregator {
    private final List<AdvertResponse> adverts;

    public ResponseAggregator() {
        this.adverts = new ArrayList<>();
    }

    public void add(AdvertResponse[] adverts) {
        this.adverts.addAll(Arrays.asList(adverts));
    }

    public List<AdvertResponse> getAdverts() {
        return new ArrayList<>(this.adverts);
    }

    public Map<String, Object> toMap(String[] fields) {
        Map<String, Object>       map        = new HashMap<>();
        Set<String>               fieldSet   = new HashSet<>(Arrays.asList(fields));
        List<Map<String, Object>> advertList = new ArrayList<>(this.adverts.size());
        for (AdvertResponse advert : this.adverts) {
            advertList.add(advert.toMap(fieldSet));
        }
        map.put("totalHits", this.adverts.size());
        map.put("adverts",   advertList);
        return map;
    }
}

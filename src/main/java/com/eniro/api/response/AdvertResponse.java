package com.eniro.api.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Eniro Response class for Advert
 * Created by zeeshan
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AdvertResponse {
    private String              eniroId;
    private String              homepage;
    private String              facebook;
    private String              companyReviews;
    private String              infoPageLink;
    private CompanyInfoResponse companyInfo;
    private AddressResponse     address;
    private LocationResponse    location;
    private PhoneResponse[]     phoneNumbers;

    private static final Field[] fields = AdvertResponse.class.getDeclaredFields();

    public String getEniroId() {
        return eniroId;
    }

    public void setEniroId(String eniroId) {
        this.eniroId = eniroId;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getCompanyReviews() {
        return companyReviews;
    }

    public void setCompanyReviews(String companyReviews) {
        this.companyReviews = companyReviews;
    }

    public String getInfoPageLink() {
        return infoPageLink;
    }

    public void setInfoPageLink(String infoPageLink) {
        this.infoPageLink = infoPageLink;
    }

    public CompanyInfoResponse getCompanyInfo() {
        return companyInfo;
    }

    public void setCompanyInfo(CompanyInfoResponse companyInfo) {
        this.companyInfo = companyInfo;
    }

    public AddressResponse getAddress() {
        return address;
    }

    public void setAddress(AddressResponse address) {
        this.address = address;
    }

    public LocationResponse getLocation() {
        return location;
    }

    public void setLocation(LocationResponse location) {
        this.location = location;
    }

    public PhoneResponse[] getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(PhoneResponse[] phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public Map<String, Object> toMap(Set<String> fieldSet) {
        Map<String, Object> map = new HashMap<>();
        for (Field field : fields) {
            String fieldName = field.getName();
            if (fieldSet.contains(fieldName)) {
                try {
                    map.put(fieldName, field.get(this));
                } catch (IllegalAccessException e) { /* ignored because it should not happen */ }
            }
        }
        return map;
    }

    @Override
    public String toString() {
        return "AdvertResponse{" +
                "companyInfo=" + this.companyInfo +
                "}";
    }
}

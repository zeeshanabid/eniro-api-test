package com.eniro.api.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Eniro Response class for Coordinate
 * Created by zeeshan
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CoordinateResponse {
    private double latitude;
    private double longitude;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "Coordinate{latitude=" + this.latitude + ", longitude=" + this.longitude + "}";
    }
}

package com.eniro.api.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Eniro Response class for Company Info
 * Created by zeeshan
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CompanyInfoResponse {
    private String companyName;
    private String orgNumber;
    private String companyText;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getOrgNumber() {
        return orgNumber;
    }

    public void setOrgNumber(String orgNumber) {
        this.orgNumber = orgNumber;
    }

    public String getCompanyText() {
        return companyText;
    }

    public void setCompanyText(String companyText) {
        this.companyText = companyText;
    }

    @Override
    public String toString() {
        return "CompanyInfo{name=" + this.companyName + "}";
    }
}

package com.eniro.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * This is the starting point of the spring application.
 * 
 * Created by zeeshan on 03/11/15.
 */
@SpringBootApplication
public class EniroApplication {
    private static final Logger logger = LoggerFactory.getLogger(EniroApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(EniroApplication.class, args);
    }
}

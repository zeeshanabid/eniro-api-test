package com.eniro.api.controller;

import com.eniro.api.response.ResponseAggregator;
import com.eniro.api.search.logger.SearchLog;
import com.eniro.api.search.logger.SearchLogger;
import com.eniro.api.search.searcher.EniroSearcher;
import com.eniro.api.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Search and log end points for the REST API
 *
 * Created by zeeshan
 */
@RestController
public class SearchController {
    private static final Logger logger = LoggerFactory.getLogger(SearchController.class);

    @Autowired
    private EniroSearcher searcher;

    @Autowired
    private SearchLogger searchLogger;

    @RequestMapping("/api/search")
    public Map<String, Object> search(
            @RequestParam(value="search-word[]", required = true) String[] searchWords,
            @RequestParam(value="fields[]",      required = true) String[] fields
    ) throws Exception {
        logger.info("Search Words: {}", Arrays.toString(searchWords));
        logger.info("Fields: {}",       Arrays.toString(fields));

        ResponseAggregator response = searcher.search(searchWords);
        searchLogger.log(searchWords, fields);
        return response.toMap(fields);
    }

    @RequestMapping("/api/search/log")
    public List<SearchLog> getLog(
            @RequestParam(value="from", required = true) String from,
            @RequestParam(value="to",   required = true) String to
    ) throws Exception {
        Date fromDate = Utils.parseDate(from);
        Date toDate   = Utils.parseDate(to);
        logger.info("Getting search log {} - {}", from, to);

        return searchLogger.getLog(fromDate, toDate);
    }
}

package com.eniro.api.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.lang.ThreadLocal;

/**
 * Some utility methods for convenience.
 *
 * Created by zeeshan
 */
public class Utils {
    // SimpleDateFormat is not thread safe. Use FastDateFormat from apache commons or ThreadLocal like this
    private static final ThreadLocal<SimpleDateFormat> dateFormat = new ThreadLocal<SimpleDateFormat>() {
        @Override protected SimpleDateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        }
    };

    private Utils() {} // do not create any object of this class

    public static Date parseDate(String date) throws ParseException {
       return dateFormat.get().parse(date);
    }

    public static String formatDate(Date date) {
        return dateFormat.get().format(date);
    }
}

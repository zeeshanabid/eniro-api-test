package com.eniro.api.controller;

import com.eniro.api.response.AdvertResponse;
import com.eniro.api.response.CompanyInfoResponse;
import com.eniro.api.response.ResponseAggregator;
import com.eniro.api.search.logger.SearchLogger;
import com.eniro.api.search.searcher.EniroSearcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by zeeshan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MockServletContext.class)
@WebAppConfiguration
public class SearchControllerTest {

    @Mock
    EniroSearcher searcher;

    @Mock
    SearchLogger searchLogger;

    @InjectMocks
    SearchController searchController;

    private MockMvc mvc;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        ResponseAggregator  agg      = new ResponseAggregator();
        AdvertResponse      advert   = new AdvertResponse();
        CompanyInfoResponse compInfo = new CompanyInfoResponse();
        compInfo.setCompanyName("Pizzeria");
        advert.setEniroId("112233");
        advert.setCompanyInfo(compInfo);
        agg.add(new AdvertResponse[] {advert});
        Mockito.when(searcher.search(Mockito.any(String[].class))).thenReturn(agg);
        mvc = MockMvcBuilders.standaloneSetup(searchController).build();
    }

    @Test
    public void getSearchHitList() throws Exception {
        mvc.perform(post("/api/search")
                .param("search-word[]", "pizza")
                .param("fields[]",      "eniroId")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("112233")));

        mvc.perform(post("/api/search")
                .param("search-word[]", "pizza")
                .param("fields[]",      "companyInfo")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Pizzeria")));

        mvc.perform(post("/api/search")
                .param("search-word[]", "pizza")
                .param("fields[]",      "companyInfo")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(not(containsString("112233"))));

        mvc.perform(post("/api/search")
                .param("search-word[]", "pizza")
                .param("fields[]", "companyInfo")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("\"totalHits\":1")));
    }
}

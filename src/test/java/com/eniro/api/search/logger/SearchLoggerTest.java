package com.eniro.api.search.logger;

import com.eniro.api.utils.TestUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Date;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * Created by zeeshan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MockServletContext.class)
@WebAppConfiguration
public class SearchLoggerTest {
    private SearchLogger searchLogger = Mockito.spy(new SearchLogger());

    @Test
    public void testLog() {
        Date     from        = TestUtils.parseDate("2015-11-05 01:01:01");
        Date     to          = TestUtils.parseDate("2015-11-05 03:30:00");
        String[] searchWords = new String[] { "pizza", "kebab" };
        String[] fields      = new String[] { "eniroId" };
        Mockito.when(searchLogger.getDate()).thenReturn(TestUtils.parseDate("2015-11-05 02:01:01"));
        searchLogger.log(searchWords, fields);
        assertArrayEquals("Search log must contains pizza & kebab search", searchWords, searchLogger.getLog(from, to).get(0).getSearchWords());

        searchWords = new String[] { "book", "store" };
        fields      = new String[] { "eniroId" };
        Mockito.when(searchLogger.getDate()).thenReturn(TestUtils.parseDate("2015-11-05 02:30:01"));
        searchLogger.log(searchWords, fields);

        String[] pastaPlace = new String[] { "pasta", "place" };
        fields              = new String[] { "eniroId" };
        Mockito.when(searchLogger.getDate()).thenReturn(TestUtils.parseDate("2015-11-05 04:30:01"));
        searchLogger.log(pastaPlace, fields);

        assertEquals("Size of search log must be 2", 2, searchLogger.getLog(from, to).size());
        assertArrayEquals("Search log must contains book & store search", searchWords, searchLogger.getLog(from, to).get(1).getSearchWords());

        to = TestUtils.parseDate("2015-11-05 05:30:00");
        assertEquals("Size of search log must be 3", 3, searchLogger.getLog(from, to).size());
    }
}

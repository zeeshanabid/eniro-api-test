package com.eniro.api.search.searcher;

import com.eniro.api.response.ResponseAggregator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

/**
 * Created by zeeshan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MockServletContext.class)
@WebAppConfiguration
public class EniroSearcherTest {

    private String exampleResponse = "{" +
            "    \"title\": \"Gula sidorna API\"," +
            "    \"query\": \"http://api.eniro.com/cs/search/basic?country=se&search_word=pizza&version=1.1.3\"," +
            "    \"totalHits\": 3681," +
            "    \"totalCount\": 3681," +
            "    \"startIndex\": 1," +
            "    \"itemsPerPage\": 25," +
            "    \"adverts\": [" +
            "        {" +
            "            \"eniroId\": \"14873025\"," +
            "            \"companyInfo\": {" +
            "                \"companyName\": \"Patricias Pizzeria\"," +
            "                \"orgNumber\": \"9697429851\"," +
            "                \"companyText\": \"Välkommen till Patricias Pizzeria i Kumla. Vi finns på Sveavägen 8 och erbjuder pizza, kebab, sallad...\"" +
            "            }," +
            "            \"address\": {" +
            "                \"streetName\": \"Sveav. 8\"," +
            "                \"postCode\": \"692 30\"," +
            "                \"postArea\": \"KUMLA\"," +
            "                \"postBox\": null" +
            "            }," +
            "            \"location\": {" +
            "                \"coordinates\": [" +
            "                    {" +
            "                        \"longitude\": 15.1429787," +
            "                        \"latitude\": 59.1256764" +
            "                    }," +
            "                    {" +
            "                        \"use\": \"route\"," +
            "                        \"longitude\": 15.1429787," +
            "                        \"latitude\": 59.1256764" +
            "                    }" +
            "                ]" +
            "            }," +
            "            \"phoneNumbers\": [" +
            "                {" +
            "                    \"type\": \"std\"," +
            "                    \"phoneNumber\": \"019 - 58 15 10\"," +
            "                    \"label\": null" +
            "                }" +
            "            ]," +
            "            \"companyReviews\": \"http://www.rejta.se/omdome/681256/pizza\"," +
            "            \"homepage\": \"http://api.eniro.com/proxy/homepage/uANwPf5aVK2jvHRqZb4lSKH3eZNTZkKMhblZAytu_jJEaWl6ODDqeOnECqxafdz_5W4DHVHqLj963KZnepB2OQ==\"," +
            "            \"facebook\": null," +
            "            \"infoPageLink\": \"http://gulasidorna.eniro.se/f/patricias-pizzeria:14873025?search_word=pizza\"" +
            "        }" +
            "    ]" +
            "}";

    private RestTemplate          restTemplate;
    private EniroSearcher         searcher;
    private MockRestServiceServer mockServer;

    @Before
    public void setup() {
        restTemplate = new RestTemplate();
        mockServer   = MockRestServiceServer.createServer(restTemplate);
        searcher     = Mockito.spy(new EniroSearcher());
        Mockito.when(searcher.createTemplate()).thenReturn(restTemplate);
    }

    @Test
    public void testSearch() {
        mockServer.expect(requestTo("null?profile=&key=&country=&version=&search_word=pizza")).andRespond(withSuccess(exampleResponse, MediaType.APPLICATION_JSON));
        ResponseAggregator response = searcher.search(new String[]{"pizza"});
        mockServer.verify();
        assertEquals("response must have exactly one advert", 1, response.getAdverts().size());
        assertEquals("response advert name must match", "Patricias Pizzeria", response.getAdverts().get(0).getCompanyInfo().getCompanyName());
    }
}
